import styled from 'styled-components'

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  gap: 1rem;
  max-width: 60rem;
`

export const WrapperButtons = styled.div`
  display: flex;
  justify-content: center;
  gap: 1rem;
`
