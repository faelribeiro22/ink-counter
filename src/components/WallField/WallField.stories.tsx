import { Story, Meta } from '@storybook/react/types-6-0'
import WallField from '.'

export default {
  title: 'WallField',
  component: WallField
} as Meta

export const Basic: Story = (args) => <WallField {...args} />
