import Button from '../Button'
import { useCallback, useState } from 'react'
import InputTextField from '../InputTextField'
import * as S from './styles'

const WallField = () => {
  const validateNumber = useCallback((value: string) => {
    const regexNumber = /^(?=.*\d)\d*[.,]?\d*$/
    return regexNumber.test(value)
  }, [])

  const [height, setHeight] = useState('')
  const [width, setWidth] = useState('')
  const [windows, setWindows] = useState('')
  const [doors, setDoors] = useState('')

  const onHandleChange = useCallback(
    (value: string, setValue: (elem: string) => void) => {
      if (validateNumber(value) || value === '') {
        setValue(value)
      }
    },
    [validateNumber]
  )

  return (
    <S.Wrapper>
      <InputTextField
        id="altura"
        label="Altura da parede"
        labelFor="altura"
        value={height}
        onChange={(e) => onHandleChange(e.target.value, setHeight)}
      />
      <InputTextField
        id="largura"
        label="Largura da parede"
        labelFor="largura"
        value={width}
        onChange={(e) => onHandleChange(e.target.value, setWidth)}
      />
      <InputTextField
        id="janelas"
        type="number"
        label="Quantidade de janelas"
        labelFor="janelas"
        value={windows}
        onChange={(e) => onHandleChange(e.target.value, setWindows)}
      />
      <InputTextField
        id="portas"
        type="number"
        label="Quantidade de portas"
        labelFor="portas"
        value={doors}
        onChange={(e) => onHandleChange(e.target.value, setDoors)}
      />
      <S.WrapperButtons>
        <Button>Adicionar</Button>
        <Button secondary>Limpar</Button>
      </S.WrapperButtons>
    </S.Wrapper>
  )
}
export default WallField
