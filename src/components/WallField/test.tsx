import { screen } from '@testing-library/react'
import { renderWithTheme } from 'utils/tests/helpers'
import WallField from '.'

describe('<WallField />', () => {
  it('should render the inputs to height and width wall', () => {
    renderWithTheme(<WallField />)
    expect(screen.getByLabelText('Altura da parede')).toBeInTheDocument()

    expect(screen.getByLabelText('Largura da parede')).toBeInTheDocument()

    expect(screen.getByLabelText('Quantidade de portas')).toBeInTheDocument()

    expect(screen.getByLabelText('Quantidade de janelas')).toBeInTheDocument()

    // expect(container.firstChild).toMatchSnapshot()
  })
})
